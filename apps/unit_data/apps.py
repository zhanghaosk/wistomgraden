from django.apps import AppConfig


class UnitDataConfig(AppConfig):
    name = 'unit_data'
    verbose_name = u"单位数据维护"