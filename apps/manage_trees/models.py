from django.db import models
permissions_choices = (
    ("state_owned", "国有"),
    ('private', '私有'),
)
class AddAncientInformation(models.Model):#添加古树信息
    level_choices = (
        ("first", "一级"),
        ('second', "二级"),
        ('third', "三级"),
    )
    tree_number = models.CharField(verbose_name="古树编号", max_length=10)#古树编号
    original_number = models.CharField(verbose_name="原编号", max_length=10, blank=True, null=True)#原编号
    survey_number = models.CharField(verbose_name="调查号", max_length=20, blank=True, null=True)#调查号
    survey_date = models.DateField(verbose_name="调查日期", auto_now_add=True, blank=True, null=True)#调查日期
    village  = models.CharField(verbose_name="乡镇", max_length=100, blank=True, null=True)#乡镇
    place = models.CharField(verbose_name="具体地点", max_length=100)#具体地点
    dominion = models.CharField(verbose_name="管辖单位", max_length=100, blank=True, null=True)#管辖单位
    permissions = models.CharField(verbose_name="权属", max_length=100, choices=permissions_choices)#权属
    #administrae_division #行政区划表
    coordinates_longitude = models.FloatField(verbose_name="经度")#经度
    coordinates_latitude = models.FloatField(verbose_name="纬度")#纬度
    special_case_description = models.CharField(verbose_name="特殊情况描述", max_length=100, blank=True, null=True)#特殊情况描述
    situation_and_advice = models.CharField(verbose_name="现状及建议", max_length=100, blank=True, null=True)#现状及建议
    investigator = models.CharField(verbose_name="调查者", max_length=50, blank=True, null=True)#调查表
    reviewer = models.CharField(verbose_name="审核人", max_length=50)#审核人
    tree_species = models.CharField(verbose_name="树种", max_length=100, choices=permissions_choices)#树种
    level = models.CharField(verbose_name="级别", max_length=100, choices=level_choices)#级别
    tree_height = models.FloatField(verbose_name="树高", blank=True, null=True)#树高
    quality = models.CharField(verbose_name="特征", max_length=100, blank=True, null=True)#特征
    chest_diameter = models.FloatField(verbose_name="胸径", blank=True, null=True)#胸径
    situation = models.CharField(verbose_name="态势", max_length=100, choices=permissions_choices, blank=True, null=True)#态势
    crown = models.FloatField(verbose_name="平均冠幅", blank=True, null=True)#平均冠幅
    # maintenance_team_id #养护队伍id
    update_time = models.DateTimeField(verbose_name="更新时间")#更新时间
    note = models.CharField(verbose_name="备注", max_length=200, blank=True, null=True)#备注
    class Meta:
        verbose_name = "添加古树信息"
        verbose_name_plural = "添加古树信息"
class AncientTreepatrol(models.Model):#古树巡查表
    tree_number = models.ForeignKey(AddAncientInformation, on_delete=models.CASCADE)
    situation = models.CharField(verbose_name="生长态势", max_length=100, choices= permissions_choices, blank=True, null=True)#生长态势
    tree_condition = models.CharField(verbose_name="树体情况", max_length=200, blank=True, null=True)#树体情况
    data = models.DateTimeField(verbose_name="巡查日期")#巡查日期
    people = models.CharField(verbose_name="巡查人", max_length=50, blank=True, null=True)#巡查人
    note = models.CharField(verbose_name="备注", max_length=200, blank=True, null=True)#备注
    class Meta:
        verbose_name = "古树巡查"
        verbose_name_plural = "古树巡查"
class AddAncientTreeMaintenance(models.Model):#古树养护
    tree_number = models.CharField(verbose_name="古树编号", max_length=10, primary_key=True)#古树养护编号
    maintenance_date = models.DateTimeField(verbose_name="养护日期", auto_now_add=True)#养护日期
    maintenance_content = models.CharField(verbose_name="养护内容", max_length=100, choices=permissions_choices, blank=True, null=True)#养护内容
    photo = models.ImageField(verbose_name="上传照片", upload_to="pic", blank=True, null=True)#上传照片
    maintenance_people = models.CharField(verbose_name="养护人", max_length=25, blank=True, null=True)#养护人
    note = models.CharField(verbose_name="备注", max_length=200, blank=True, null=True)#备注
    class Meta:
        verbose_name = "添加古树养护"
        verbose_name_plural = "添加古树养护"
class AddAncientTreeRejuvenation(models.Model):#古树复壮
    rejuvenation_number = models.IntegerField(verbose_name="复壮编号", primary_key=True)#复壮编号
    tree_number = models.ForeignKey(AddAncientInformation,on_delete=models.CASCADE)
    rejuvenation_date = models.DateTimeField(verbose_name="复壮日期", auto_now_add=True)#复壮日期
    rejuvenation_content = models.CharField(verbose_name="复壮内容", max_length=100, choices=permissions_choices, blank=True, null=True)#复壮内容
    rejuvenation_people = models.CharField(verbose_name="复壮人", max_length=50, blank=True, null=True)#复壮人
    photo = models.ImageField(verbose_name="上传图片", upload_to="pic1", blank=True, null=True)#图片上传
    note = models.CharField(verbose_name="备注", max_length=200, blank=True, null=True)#备注
    class Meta:
        verbose_name = "添加古树复壮"
        verbose_name_plural = "添加古树复壮"
class AncientTreeadopt(models.Model):#古树认养
    tree_number = models.ForeignKey(AddAncientInformation, models.CASCADE)
    applicant = models.CharField(verbose_name="申请单位/人", max_length=50, blank=True, null=True)#申请单位/人
    people = models.CharField(verbose_name="联系人", max_length=50, blank=True, null=True)#联系人
    phone = models.CharField(verbose_name="联系电话", max_length=15, blank=True, null=True)#联系电话
    application_date = models.DateTimeField(verbose_name="申请日期")#申请日期
    note = models.CharField(verbose_name="备注", max_length=200, blank=True, null=True)#备注
    class Meta:
        verbose_name = "添加古树认养"
        verbose_name_plural = "添加古树认养"
class AncientInformationStatistics(models.Model):#古树信息统计
    species_statistics = models.IntegerField(verbose_name="古树统计")#古树统计
    level_statistics = models.IntegerField(verbose_name="级别统计")#级别统计
    growth_momentum_statistics = models.IntegerField(verbose_name="生长态势统计")#生长态势统计
    growing_environment_statistics = models.IntegerField(verbose_name="生长环境统计")#生长环境统计
    ancient_information_statistics = models.IntegerField(verbose_name="古树信息统计")#古树信息统计
    note = models.CharField(verbose_name="备注", max_length=200)#备注
    class Meta:
        verbose_name = "古树信息统计"
        verbose_name_plural = "古树信息统计"

# Create your models here.
