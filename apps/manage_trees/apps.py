from django.apps import AppConfig


class ManageTreesConfig(AppConfig):
    name = 'manage_trees'
