import xadmin
from . import models
class AddAncientInformationAdmin(object):
    list_display = ("id", "tree_number", "tree_species", "level", "situation",
                  "place", "dominion", "update_time")
    list_display_links = ( "tree_number", )
    list_filter = ("id","place")
    search_fields = ("id","place")
# Register your models here.
xadmin.site.register(models.AddAncientInformation,AddAncientInformationAdmin)#添加古树信息
xadmin.site.register(models.AddAncientTreeMaintenance)#古树养护
xadmin.site.register(models.AddAncientTreeRejuvenation)#古树巡查表
xadmin.site.register(models.AncientInformationStatistics)#古树复壮
xadmin.site.register(models.AncientTreeadopt)#古树认养
xadmin.site.register(models.AncientTreepatrol)#古树信息统计